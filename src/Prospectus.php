<?php

namespace DNB;

class Prospectus extends AbstractPostSerializable implements PostSerializableInterface {

    private Assignment $assigment;
    private Questionaire $questionaire;
    private Customer $customer;

    public function __construct(
        Assignment $assigment,
        Questionaire $questionaire,
        Customer $customer
    ) {
        $this->assigment = $assigment;
        $this->questionaire = $questionaire;
        $this->customer = $customer;
    }

    public function toPostParams(): array {
        return array_merge(
            $this->assigment->toPostParams(),
            $this->questionaire->toPostParams(),
            [
                'customer' => $this->customer->toPostParams()
            ]
        );
    }

}
