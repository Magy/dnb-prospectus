<?php

namespace DNB;

class Assignment extends AbstractPostSerializable implements PostSerializableInterface {
    
    private string $assignmentNumber;

    public function __construct(
        string $assignmentNumber
    ){
        $this->assignmentNumber = $assignmentNumber;
    }

    public function toPostParams(): array {
        return ['assignmentNumber' => $this->assignmentNumber];
    }
}
