<?php

namespace DNB;

final class ClientConfig {

    private string $id;
    private string $secret;
    private bool $is_test;

    public function __construct(
        string $id,
        string $secret,
        bool $is_test = true
    ) {
        $this->id = $id;
        $this->secret = $secret;
        $this->is_test = $is_test;
    }

    public function get(): object {
        return new class (
            $this->id,
            $this->secret,
            $this->is_test ? 'uat-process-externalpart' : 'dnb-process-externalpart',
            $this->is_test ? 'uat-nextgen-identityserver' : 'dnb-nextgen-identityserver',
        ) {
            public string $id;
            public string $secret;
            public string $endpoint;
            public string $auth_endpoint;

            public function __construct(
                string $id,
                string $secret,
                string $endpoint,
                string $auth_endpoint
            ) { 
                $this->id = $id;
                $this->secret = $secret;
                $this->endpoint = $endpoint;
                $this->auth_endpoint = $auth_endpoint;
            }
        };
    }
}
