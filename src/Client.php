<?php

namespace DNB;

use HaydenPierce\ClassFinder\ClassFinder;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Client {
    private HttpClientInterface $client;
    private ClientConfig $clientConfig;
    private object $auth;

    public function __construct(ClientConfig $clientConfig) {
        $this->clientConfig = $clientConfig;
        $this->client = HttpClient::create(
            [
                'headers' => [
                    'cache-control' => 'no-cache',
                ]
            ]
        );
        $this->auth = $this->authenticate();
    }

    private function getUrl(string $endpoint, string $path): string {
        return sprintf(
            'https://%s.azurewebsites.net/%s', rtrim($endpoint, '/'), ltrim($path, '/')
        );
    }

    public function authenticate(): object {
        $request = $this->client->request(
            'POST',
            $this->getUrl($this->clientConfig->get()->auth_endpoint, '/connect/token'), [
                'body' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->clientConfig->get()->id,
                    'client_secret' => $this->clientConfig->get()->secret
                ]
            ]
        );

        $result = $request->toArray();

        return new class (
            $result['access_token'],
            $result['token_type']
        ) {
            private string $token;
            private string $type;
            public function __construct(
                string $token,
                string $type
            ) {
                $this->token = $token;
                $this->type = $type;
            }

            public function getHeader(): string {
                return sprintf("%s %s", $this->type, $this->token);
            }
        };
    }

    private function getAuthClient(): HttpClientInterface {
        return $this->client->withOptions([
            'headers' => [
                'Authorization' => $this->auth->getHeader(),
            ]
        ]);
    }

    private function findInNamespace(string $ns): ?string {
        $classes = array_filter(ClassFinder::getClassesInNamespace(
            dirname(str_replace('\\', DIRECTORY_SEPARATOR, self::class))
        ), function (string $class) {
            return class_implements($class, AbstractPostSerializable::class) && strpos($class, AbstractPostSerializable::class) !== 0;
        });

        $key = array_search(strtolower($ns), array_map(function (string $class): string {
            return $class::namespace();
        }, $classes));

        return $key ? $classes[$key] : null;
    }

    public function orderProspectus(
        Prospectus $prospectus
    ): bool {
        $ret = $this->getAuthClient()->request(
            "POST",
            $this->getUrl($this->clientConfig->get()->endpoint, '/api/v1/autoprospect/orderprospect'), [
                'json' => $prospectus->toPostParams(),
            ]
        );
        try {
            $ret->getContent();
            return true;
        } catch (\Exception $e) {
            $content = json_decode($ret->getContent(false), true);

            if (json_last_error() == JSON_ERROR_NONE) {
                foreach ($content as $key => $value) {
                    $scope = explode('.', $key);

                    if (sizeof($scope) <= 1)
                        continue;

                    $ns = $this->findInNamespace($scope[0]);

                    foreach (array_keys($ns::getPostParams()) as $param) {
                        if (strpos(strtolower($param), strtolower($scope[1])) == strlen($ns::namespace())+1) {
                            unset($content[$key]);
                            $content[$param] = $value;
                        }
                    }

                }
                $content = json_encode($content);
            } else
                $content = $ret->getContent(false);

            throw new \InvalidArgumentException($content ? $content : $e->getMessage(), $e->getCode(), $e);
        }

        return false;
    }
}
