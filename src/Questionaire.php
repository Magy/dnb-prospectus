<?php

namespace DNB;

class Questionaire extends AbstractPostSerializable implements PostSerializableInterface {

    private ?bool $keepMeInformedOnBids;
    private ?bool $contactForPropertyValuation;
    private ?bool $contactForEconomicalAdvice;

    public function __construct(
        ?bool $keepMeInformedOnBids = null,
        ?bool $contactForPropertyValuation = null,
        ?bool $contactForEconomicalAdvice = null
    ){
        $this->keepMeInformedOnBids = $keepMeInformedOnBids;
        $this->contactForPropertyValuation = $contactForPropertyValuation;
        $this->contactForEconomicalAdvice = $contactForEconomicalAdvice;
    }

    public function toPostParams(): array {
        $arr = [];

        foreach (array_keys(static::getPostParams(true)) as $key) {
            $arr[$key] = !is_null($this->{$key}) ? $this->{$key} : false;
        }

        return $arr;
    }

}
