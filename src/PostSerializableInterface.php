<?php

namespace DNB;

interface PostSerializableInterface {
    public function toPostParams(): array;
    public static function fromPostParams(array $post): self;
    public static function getPostParams();
}
