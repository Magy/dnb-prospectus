<?php

namespace DNB;

class Customer extends AbstractPostSerializable implements PostSerializableInterface {
    private string $name;
    private ?string $email;
    private string $mobilePhone;
    private ?string $zipCode;
    private ?string $comment;

    public function __construct(
        string $name,
        string $mobilePhone,
        ?string $email = null,
        ?string $zipCode = null,
        ?string $comment = null
    ) {
        $this->name = $name;
        $this->email = $email;
        $this->mobilePhone = $mobilePhone;
        $this->zipCode = $zipCode;
        $this->comment = $comment;
    }

    public function toPostParams(): array {
        $arr = [];

        foreach (array_keys(static::getPostParams(true)) as $key)
            if (!is_null($this->{$key}))
                $arr[$key] = $this->{$key};

        return $arr;
    }

}
